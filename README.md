# Certbot DNS Plugin for Gandi LiveDNS

It's a simple shell script for certbot to create/renew/download SSL certificates.

If you register a domain name in Gandi and using Gandi LiveDNS as nameservers. You can download this script and get your let's encrypt SSL using certbot.

# Usage

1. git clone git@gitlab.com:haway/certbot-plugin-dns-gandi.git /etc/letsencrypt/gandi
2. Get your LiveDNS APIKEY from "Security" tab in [Gandi Account](https://account.gandi.net).
3. Replace "APIKEY" in "authenticator.sh".
4. Setup execute permission (chmod u+x authenticator.sh).
4. Enjoy it!

> certbot certonly --manual --preferred-challenges=dns --manual-auth-hook /etc/letsencrypt/gandi/authenticator.sh -d example.com.tw

It's supports subdomain, for example: 

Your domain name is: example.com  
Get a SSL for "sub1.sub1.example.com":

> certbot certonly --manual --preferred-challenges=dns --manual-auth-hook /etc/letsencrypt/gandi/authenticator.sh -d sub1.sub2.example.com
